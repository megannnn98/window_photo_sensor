#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <string_view>
#include <lightcontroller.hpp>
#include <memory>
#include <stdexcept>

LightController lc({});

TEST(LightController, sunday)
{
	std::uint8_t weekday = 0;
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 0, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 0, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 1, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 1, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 2, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 2, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 3, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 3, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 4, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 4, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 5, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 5, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 6, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 6, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 7, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 7, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 8, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 8, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 8, 59));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 9, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 21, 59));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 22, 00));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 22, 01));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 22, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 23, 00));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 23, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 23, 59));
}
TEST(LightController, ordinaryDays)
{
	for (std::uint8_t weekday = 1; weekday < 5; weekday++) {
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 0, 0));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 0, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 1, 0));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 1, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 2, 0));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 2, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 3, 0));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 3, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 4, 0));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 4, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 5, 0));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 5, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 6, 00));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 6, 29));
		EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
						checkWhatToDoFrnd(lc, weekday, 6, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
						checkWhatToDoFrnd(lc, weekday, 6, 31));
		EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
						checkWhatToDoFrnd(lc, weekday, 21, 59));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 22, 00));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 22, 01));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 22, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 23, 00));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 23, 30));
		EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
						checkWhatToDoFrnd(lc, weekday, 23, 59));
	} // end for 
}

TEST(LightController, friday)
{
	std::uint8_t weekday = 5;
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 0, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 0, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 1, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 1, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 2, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 2, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 3, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 3, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 4, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 4, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 5, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 5, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 6, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 6, 29));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 6, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 6, 31));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 22, 59));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 23, 00));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 23, 01));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 23, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 23, 59));
}

TEST(LightController, saturday)
{
	std::uint8_t weekday = 6;
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 0, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 0, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 1, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 1, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 2, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 2, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 3, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 3, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 4, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 4, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 5, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 5, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 6, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 6, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 7, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 7, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 7, 59));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 8, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 9, 0));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 21, 59));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 22, 00));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 22, 01));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 22, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 23, 00));
	EXPECT_TRUE(Lamp::LampState::eLAMP_ON ==
					checkWhatToDoFrnd(lc, weekday, 23, 30));
	EXPECT_TRUE(Lamp::LampState::eLAMP_OFF ==
					checkWhatToDoFrnd(lc, weekday, 23, 59));
}

TEST(LightController, wrongDays)
{
  EXPECT_THROW(checkWhatToDoFrnd(lc, 7, 0, 0), std::invalid_argument);
  EXPECT_THROW(checkWhatToDoFrnd(lc, 0, 24, 0), std::invalid_argument);
  EXPECT_THROW(checkWhatToDoFrnd(lc, 0, 0, 60), std::invalid_argument);
}
