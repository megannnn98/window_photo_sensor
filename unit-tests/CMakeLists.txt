cmake_minimum_required(VERSION 3.21)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
project(tests)

include(CTest)

add_subdirectory(googletest)
target_compile_features(gtest PUBLIC cxx_std_20)
target_compile_features(gtest_main PUBLIC cxx_std_20)
target_compile_features(gmock PUBLIC cxx_std_20)
target_compile_features(gmock_main PUBLIC cxx_std_20)
include(GoogleTest)

add_library(options INTERFACE)
target_compile_features(options INTERFACE c_std_11 cxx_std_20)
target_compile_options(
  options
  INTERFACE -g
            -O0
            --coverage
            -Wall
            -Wextra
            -Wpedantic
            -Werror)
target_link_options(options INTERFACE --coverage)

add_subdirectory(mocks)
add_subdirectory(tests)

