#pragma once
#include <gmock/gmock.h>

#include <cstdint>
class Lamp
{
public:
  enum struct LampState : std::uint8_t
  {
      eLAMP_OFF = 0x00U,
      eLAMP_ON = 0x01U,
  };
  Lamp(std::size_t, std::size_t){}

  MOCK_METHOD(LampState, GetPinState, ());
  MOCK_METHOD(void, InputToRelay, ());
  MOCK_METHOD(void, Off, ());
  MOCK_METHOD(void, On, ());
};
