#include <lamp.hpp>
#include <iostream>
#include <thread>
#include <string>
#include <mutex>
#include <functional>
#include <plog/Log.h> // Step1: include the headers
#include "plog/Initializers/RollingFileInitializer.h"
#include "connectionkeeper.hpp"
#include "lightcontroller.hpp"
#if USE_SERIAL
#include "serialreader.hpp"
#endif

static constexpr std::size_t RELAY_PIN = 3U;
static constexpr std::size_t ARDINO_STATUS_PIN = 5U;
//static constexpr std::string_view portName {"/dev/ttyUSB0"};
static constexpr std::size_t serialBaud {115200};

void print_log(std::string_view s) {
	static std::mutex mut_a;
	std::lock_guard < std::mutex > la { mut_a };
	std::cout << s << std::endl << std::flush;
	PLOG(plog::debug) << s;
}

extern "C" int main()
try {
	plog::init(plog::debug, "log.txt");
	print_log("start");

	auto lamp = std::make_unique < Lamp > (RELAY_PIN, ARDINO_STATUS_PIN);

	LightController::getInstance(std::move(lamp), print_log);
	ConnetionKeeper::getInstance(ROUTER_IP, THREAD_DELAY, print_log);

#if USE_SERIAL
	auto serial = std::make_shared < Serial > ("/dev/ttyUSB0", serialBaud);
	SerialReader::getInstance(serial, print_log);
#endif

	while (true)
	{
		std::this_thread::sleep_for(std::chrono::seconds(1U));
	}

	return 0;
}
catch (const std::exception &e) {
	print_log(std::string { "Unhandled exception" } + std::string { e.what() });
	std::this_thread::sleep_for(std::chrono::seconds { 5U });
}

