#!/bin/bash

#rm -rf out
rm -f ./out/build/CMakeCache.txt
rm -f ./out/build/main/flowers
rm -f /home/pi/window_photo_sensor/flowers

cmake -S . -B ./out/build/ -DUSE_SERIAL=OFF 
make -C ./out/build/
cp ./out/build/main/flowers /home/pi/window_photo_sensor/

echo "done!"

