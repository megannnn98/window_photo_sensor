#pragma once

#include "serial.hpp"
#include <memory>
#include <functional>

class SerialReader
{
	std::shared_ptr<Serial> m_serial;
    std::function<void(std::string)> m_logger;

	SerialReader(std::shared_ptr<Serial> serial, std::function<void(std::string)> logger);
public:
	static SerialReader& getInstance(std::shared_ptr<Serial> serial = nullptr,
			std::function<void(std::string)> logger = [](std::string){});
};

