
#include <serialreader.hpp>
#include <thread>

SerialReader::SerialReader(std::shared_ptr<Serial> serial,
							std::function<void(std::string)> logger)
: m_serial(serial), m_logger(logger)
{
	std::thread t([](void *arg)
	{
		SerialReader& self = *static_cast<SerialReader*>(arg);

		self.m_logger("in while true SerialReader");
		while (true)
		{
			if (self.m_serial->DataAvail())
			{
				std::string s{};
				while(self.m_serial->DataAvail())
					s += self.m_serial->Getchar();

				self.m_logger(s);
			}
		}
	}, this);

	t.detach();
}

SerialReader& SerialReader::getInstance(std::shared_ptr<Serial> serial,
		std::function<void(std::string)> logger)
{
	static SerialReader instance(serial, logger);
	return instance;
}
