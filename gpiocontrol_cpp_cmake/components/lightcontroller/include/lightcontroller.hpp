#pragma once 

#include <string>
#include <functional>
#include <memory>
#include <lamp.hpp>
#include <algorithm>

class LightController
{
	static constexpr const auto CHECK_PERIOD = 60U;
	static constexpr const std::size_t DEBOUCE_TIMEOUT = 120U;
	static constexpr unsigned MINUTES_IN_HOUR = 60U;
	static constexpr unsigned DAYS_IN_WEEK = 7U;

	enum State: std::uint8_t
	{
		ePASS_DIRECTLY = 0x00U,
		eMAYBE_CHAGE   = 0x01U,
	};


	struct TH
	{
		unsigned m;
		unsigned e;

		TH(unsigned mh, unsigned mm, unsigned eh, unsigned em)
		: m{mh*MINUTES_IN_HOUR + mm},	e{eh*MINUTES_IN_HOUR + em}
		{
			m = std::clamp<unsigned>(m, 0, (24 * MINUTES_IN_HOUR - 1));
			e = std::clamp<unsigned>(e, 0, (24 * MINUTES_IN_HOUR - 1));
		}
	};


	std::array<TH, DAYS_IN_WEEK> m_timeTable{TH(9,  0, 22,  0),
																					 TH(6, 30, 22,  0),
																					 TH(6, 30, 22,  0),
																					 TH(6, 30, 22,  0),
																					 TH(6, 30, 22,  0),
																					 TH(6, 30, 23,  0),
																					 TH(8,  0, 23, 59)};

	std::unique_ptr<Lamp> m_lamp;
	std::function<void(std::string)> m_logger;


  Lamp::LampState checkWhatToDo(std::uint8_t, std::uint8_t, std::uint8_t) const;
	static std::uint32_t GetTimeNow();
	void m_TaskProcess();
	void FillFromFile(std::string filename);

public:

	friend Lamp::LampState checkWhatToDoFrnd(LightController&, std::uint8_t wd, std::uint8_t h, std::uint8_t m);
	friend void m_TaskProcessFrnd(LightController&);

	LightController(const LightController&) = delete;
	LightController& operator=(const LightController&) = delete;
	LightController(LightController&&) noexcept = delete;
	LightController& operator=(LightController&&) = delete;
	~LightController() noexcept          = default;
	LightController() noexcept          = default;

	LightController(std::unique_ptr<Lamp>, std::function<void(std::string)>);
	static LightController& getInstance(std::unique_ptr<Lamp> lamp = nullptr,
										std::function<void(std::string)> logger = [](std::string){});
};
