#include <lamp.hpp>
#include <lightcontroller.hpp>
#include <thread>
#include <iostream>
#include <stdexcept>
#include <time.h>
#include <sstream>
#include <iomanip>
#include <string>
#include <nlohmann/json.hpp>
#include <fstream>
#include <charconv>

void LightController::FillFromFile(std::string filename)
{
    std::stringstream fstream;
    std::ifstream file(filename, std::ifstream::in);
    std::vector<std::string> morning;
    std::vector<std::string> evening;
    const std::array<std::string, DAYS_IN_WEEK> week_day{"sunday", "monday", "thursday",
                                                         "wednesday", "thursday",
                                                         "friday", "saturday"};

    auto split = [](const std::string &s, char delimiter)
    {
        std::vector<std::string> tokens;
        std::string token;
        std::istringstream tokenStream(s);
        while (std::getline(tokenStream, token, delimiter))
            tokens.push_back(token);
        return tokens;
    };

    if (!file)
        throw std::runtime_error{"Error reading from file"};

    fstream << file.rdbuf();

    nlohmann::json j_complete = nlohmann::json::parse(fstream);
    file.close();
    for (unsigned i = 0; i < DAYS_IN_WEEK; ++i)
    {
        if (!j_complete[week_day[i]]["evening"].is_string())
            throw std::runtime_error{"Parsing error 1"};
        if (!j_complete[week_day[i]]["evening"].is_string())
            throw std::runtime_error{"Parsing error 2"};

        morning = split(j_complete[week_day[i]]["morning"].get<std::string>(), ':');
        evening = split(j_complete[week_day[i]]["evening"].get<std::string>(), ':');

        unsigned tmp0{};
        if (auto [p, ec] =
                std::from_chars(morning.at(0).c_str(),
                                morning.at(0).c_str() + std::strlen(morning.at(0).c_str()), tmp0);
            ec != std::errc{})
            throw std::runtime_error{"Parsing error 3"};
        unsigned tmp1{};
        if (auto [p, ec] =
                std::from_chars(morning.at(1).c_str(),
                                morning.at(1).c_str() + std::strlen(morning.at(1).c_str()), tmp1);
            ec != std::errc{})
            throw std::runtime_error{"Parsing error 4"};
        unsigned tmp2{};
        if (auto [p, ec] =
                std::from_chars(evening.at(0).c_str(),
                                evening.at(0).c_str() + std::strlen(evening.at(0).c_str()), tmp2);
            ec != std::errc{})
            throw std::runtime_error{"Parsing error 5"};
        unsigned tmp3{};
        if (auto [p, ec] =
                std::from_chars(evening.at(1).c_str(),
                                evening.at(1).c_str() + std::strlen(evening.at(1).c_str()), tmp3);
            ec != std::errc{})
            throw std::runtime_error{"Parsing error 6"};

				std::cout << week_day[i] << " "<< tmp0 << ":" << tmp1 << " " << tmp2 << ":" << tmp3 << std::endl;
				m_timeTable.at(i) = TH(tmp0, tmp1, tmp2, tmp3);
    }
}

std::uint32_t LightController::GetTimeNow()
{
	return static_cast<std::uint32_t>(std::chrono::duration_cast<std::chrono::seconds>(
																				std::chrono::time_point_cast<std::chrono::seconds>(
																						std::chrono::high_resolution_clock::now())
																						.time_since_epoch())
																				.count());
}

LightController &LightController::getInstance(std::unique_ptr<Lamp> lamp,
																							std::function<void(std::string)> logger)
{
	static LightController instance(std::move(lamp), logger);
	return instance;
}

void LightController::m_TaskProcess()
{
	static Lamp::LampState prevState = this->m_lamp->GetPinState();
	static State state = State::ePASS_DIRECTLY;
	static std::uint32_t timeOfSwitch = GetTimeNow();
	auto now = time(NULL);
	struct tm *timeinfo = localtime(&now);
	std::stringstream ss;

	ss << timeinfo->tm_wday << " " << timeinfo->tm_hour << " " << timeinfo->tm_min;
	this->m_logger(ss.str());

	if (checkWhatToDo(timeinfo->tm_wday, timeinfo->tm_hour, timeinfo->tm_min) 
																											== Lamp::LampState::eLAMP_OFF)
	{
		this->m_logger("lamp Off");
		this->m_lamp->Off();
		return;
	}

	switch (state)
	{
	case State::ePASS_DIRECTLY:
	{
		if (prevState == this->m_lamp->GetPinState())
			this->m_lamp->InputToRelay();
		else
		{
			this->m_logger("arduino pin change");
			prevState = this->m_lamp->GetPinState();
			timeOfSwitch = GetTimeNow();
			state = State::eMAYBE_CHAGE;
		}
		break;
	}
	case State::eMAYBE_CHAGE:
	{
		if ((timeOfSwitch + 60U) > GetTimeNow()) // early
		{
			if (prevState != this->m_lamp->GetPinState())
			{
				this->m_logger("arduino pin change too early");
				prevState = this->m_lamp->GetPinState();
				timeOfSwitch = GetTimeNow();
			}
		}
		else
		{
			this->m_logger("arduino pin change OK");
			prevState = this->m_lamp->GetPinState();
			state = State::ePASS_DIRECTLY;
		}
		break;
	}
	} // endswitch
}


Lamp::LampState LightController::checkWhatToDo(std::uint8_t weekday, 
																								std::uint8_t hour, 
																								 std::uint8_t minute) const
{
    if (!(weekday < 7 && hour < 24 && minute < 60))
        throw std::invalid_argument { "invalid argument" };

    unsigned all_mins = hour*MINUTES_IN_HOUR + minute;
    if (all_mins < m_timeTable[weekday].m || all_mins >= m_timeTable[weekday].e)
        return Lamp::LampState::eLAMP_OFF;

    return Lamp::LampState::eLAMP_ON;
}

LightController::LightController(std::unique_ptr<Lamp> lamp, std::function<void(std::string)> logger)
		: m_lamp{std::move(lamp)}, m_logger(logger)
{

	try 
	{
		std::cout << "start read file" << std::endl;
		FillFromFile("/home/pi/window_photo_sensor/gpiocontrol_cpp_cmake/config.dat");
		std::cout << "read file ok" << std::endl;
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception:" << e.what() << std::endl;
		this->m_logger( "Timetable will use the default settings" );
	}

	std::for_each(m_timeTable.begin(), m_timeTable.end(), 
															[](TH& tt){ std::cout << tt.m << " " << tt.e << std::endl; });

	std::thread t(
			[](void *arg)
			{
				LightController &self = *static_cast<LightController *>(arg);

				self.m_lamp->Off();
				std::this_thread::sleep_for(std::chrono::seconds(3U));
				self.m_lamp->On();
				std::this_thread::sleep_for(std::chrono::seconds(1U));
				self.m_lamp->Off();
				std::this_thread::sleep_for(std::chrono::seconds(1U));
				self.m_lamp->InputToRelay();

				self.m_logger("in while true LightController");
				while (true)
				{
					std::this_thread::sleep_for(std::chrono::seconds(60U));
					try {
						self.m_TaskProcess();
					} catch (const std::invalid_argument& ia) {
						self.m_logger(std::string( "Invalid argument: ") + ia.what());
					}
				}
			},
			this);

	t.detach();
}

Lamp::LampState checkWhatToDoFrnd(LightController& lc, 
																		std::uint8_t wd, 
																			std::uint8_t h, 
																				std::uint8_t m)
{
	return lc.checkWhatToDo(wd, h, m);
}
void m_TaskProcessFrnd(LightController& lc)
{
	return lc.m_TaskProcess();
}
