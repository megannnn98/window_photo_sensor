#pragma once
#include <cstdint>
#include <array>
#include <string>

class Lamp
{
private:

    std::size_t m_relay_pin;
    std::size_t m_arduino_pin;

public:
    enum struct LampState : std::uint8_t
    {
        eLAMP_OFF = 0x00U,
        eLAMP_ON = 0x01U,
    };

    Lamp(std::size_t, std::size_t);
    Lamp(const Lamp &) = delete;
    Lamp &operator=(const Lamp &) = delete;
    Lamp(Lamp &&) noexcept = default;
    Lamp &operator=(Lamp &&) noexcept = delete;
    ~Lamp() noexcept = default;
    void Off();
    void On();
    void Set(LampState);
    void InputToRelay();
    LampState GetPinState() const;
};
