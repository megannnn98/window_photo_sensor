
#include <lamp.hpp>
#include <wiringPi.h>
#include <stdexcept>
#include <iostream>

Lamp::Lamp(std::size_t relay_pin, std::size_t arduino_pin) : m_relay_pin(relay_pin), m_arduino_pin(arduino_pin)
{
    wiringPiSetupPhys();
    pinMode(m_relay_pin, OUTPUT);
    pinMode(m_arduino_pin, INPUT);
}

void Lamp::Off()
{
    digitalWrite(m_relay_pin, static_cast<int>(LampState::eLAMP_OFF));
}

void Lamp::On()
{
    digitalWrite(m_relay_pin, static_cast<int>(LampState::eLAMP_ON));
}

void Lamp::Set(Lamp::LampState new_state)
{
    digitalWrite(m_relay_pin, static_cast<int>(new_state));
}

void Lamp::InputToRelay()
{
    Set(static_cast<LampState>(digitalRead(m_arduino_pin)));
}

Lamp::LampState Lamp::GetPinState() const
{
    return static_cast<LampState>(digitalRead(m_arduino_pin));
}
