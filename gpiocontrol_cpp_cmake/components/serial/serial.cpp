#include <stdexcept>
#include <wiringPi.h>
#include <wiringSerial.h>
#include <serial.hpp>

Serial::Serial(std::string portName, std::size_t serialBaud)
: m_portName(portName), m_baud(serialBaud)
{
	if ((m_handler = serialOpen(portName.c_str(), serialBaud)) < 0)
		throw std::runtime_error {"Unable to open serial device"};
}
bool Serial::DataAvail() const {
	return (serialDataAvail(m_handler) > 0);
}

std::size_t Serial::Getchar() const {
	return serialGetchar(m_handler);
}
