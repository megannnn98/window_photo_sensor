#pragma once

#include <string>

class Serial {
	std::string m_portName;
	std::size_t m_baud;
	std::size_t m_handler;

public:
	Serial(std::string portName, std::size_t serialBaud);
	bool DataAvail() const ;
	std::size_t Getchar() const;
};
