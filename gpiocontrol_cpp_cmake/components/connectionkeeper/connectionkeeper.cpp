#include "connectionkeeper.hpp"
#include <thread>

ConnetionKeeper::ConnetionKeeper(const std::string routerIP, std::uint32_t threadDelay, std::function<void(std::string)> logger)
{
    m_threadDelay = threadDelay;
    m_sping = std::string{ "ping -c1 -s1 " } + routerIP;
    m_logger = logger;

    std::thread t (
    [](void* arg)
    {
        ConnetionKeeper& ck = *static_cast<ConnetionKeeper*>(arg);
        int errPingCnt = 0;
        ck.m_logger("in while true ConnetionKeeper");
        while(true)
        {
            std::this_thread::sleep_for(std::chrono::seconds(ck.m_threadDelay));
 
            if (system(ck.m_sping.c_str()))
                errPingCnt++;
            else
                errPingCnt = 0;
 
            if (errPingCnt >= 10)
            {
                ck.m_logger("internet connx failed");
                system("reboot now");
            }
        }
    }, this);

    t.detach();
}

ConnetionKeeper& ConnetionKeeper::getInstance(const std::string routerIP, 
                                    std::uint32_t threadDelay, 
                                    std::function<void(std::string)> logger)
{
    static ConnetionKeeper instance(routerIP, threadDelay, logger);
    return instance;
}

