#pragma once 

#include <string>
#include <functional>

static constexpr int THREAD_DELAY = 60U;
static const std::string ROUTER_IP = "192.168.1.1";

class ConnetionKeeper
{
    std::string m_sping;
    std::uint32_t m_threadDelay;
    std::function<void(std::string)> m_logger;

    ConnetionKeeper(const std::string, std::uint32_t, std::function<void(std::string)>);

    public:
    ConnetionKeeper(ConnetionKeeper&) = delete;
    ConnetionKeeper& operator=(const ConnetionKeeper&) = delete;
    ConnetionKeeper(ConnetionKeeper&&)                 = delete;
    ConnetionKeeper& operator=(ConnetionKeeper&&) = delete;
    ~ConnetionKeeper() noexcept          = default;
    static ConnetionKeeper& getInstance(const std::string routerIP = ROUTER_IP, 
                                        std::uint32_t threadDelay = THREAD_DELAY, 
                                        std::function<void(std::string)> logger = [](std::string){});
};
